﻿HashSet<int> CONJUNTO1 = new HashSet<int> { 1, 2, 3, 4, 6 };
HashSet<int> CONJUNTO2 = new HashSet<int> { 3, 5, 6, 7, 9 };

Console.WriteLine("Conjunto A: " + string.Join(", ", CONJUNTO1));
Console.WriteLine("Conjunto B: " + string.Join(",", CONJUNTO2));

//Unión de Conjuntos
HashSet<int>union=new HashSet<int>(CONJUNTO1);
union.UnionWith(CONJUNTO2);
Console.WriteLine("Unión de conjuntos A y B: " + string.Join(",",union));

//Intersección de conjuntos
HashSet<int> interseccion = new HashSet<int>(CONJUNTO1);
interseccion.IntersectWith(CONJUNTO2);
Console.WriteLine("Intersección de conjuntos A y B: " + string.Join(",", interseccion));

//Diferencia de conjntos (A - B)
HashSet<int> diferenciaAB = new HashSet<int>(CONJUNTO1);
diferenciaAB.ExceptWith(CONJUNTO2);
Console.WriteLine("Diferencia de conjuntos (A - B): " + string.Join(",", diferenciaAB));

//Diferencia de conjntos (B - A)
HashSet<int> diferenciaBA = new HashSet<int>(CONJUNTO2);
diferenciaBA.ExceptWith(CONJUNTO1);
Console.WriteLine("Diferencia de conjuntos (B - A): " + string.Join(",", diferenciaBA));

//Complemento del conjunto A
HashSet<int> complementoA = new HashSet<int>(CONJUNTO2);
complementoA.ExceptWith(CONJUNTO1);
Console.WriteLine("Complemento del conjunto A': " + string.Join(",", complementoA));

//Complemento del conjunto B
HashSet<int> complementoB = new HashSet<int>(CONJUNTO1);
complementoB.ExceptWith(CONJUNTO2);
Console.WriteLine("Complemento del conjunto B': " + string.Join(",", complementoB));



//Producto cartesiano
Console.WriteLine("Producto Cartesiano de los conjuntos A y B: ");

foreach (var elementoA in CONJUNTO1)
{
    foreach (var elementoB in CONJUNTO2)
    {
        Console.WriteLine($"({elementoA}, {elementoB})");
    }
}